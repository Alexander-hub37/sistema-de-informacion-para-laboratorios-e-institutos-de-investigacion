<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Equipo>
 */
class EquipoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'registro_id' => 1,
            'nombre_imagen' => $this->faker->word,
            'imagen_equipo' => $this->faker->imageUrl(),
            'nombre' => $this->faker->word,
            'marca_modelo' => $this->faker->word,
            'fecha_adquisicion' => $this->faker->date(),
            'codigo_patrimonio' => $this->faker->word,
            'accesorio' => $this->faker->word,
            'insumos' => $this->faker->word,
            'description' => $this->faker->text,
            'estado' => true, //
        ];
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Seeder para ingresar datos en la tabla Usuarios en la base de datos
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'correo' => 'admin@unsa.edu.pe',
          'contrasena' => Hash::make('password'), // password
          'dni' => '77777777',
          'nombre' => 'Admin',
          'apellido_paterno' => 'ApellidoPa',
          'apellido_materno' => 'ApellidoMa',
          'direccion' => 'Av Parra',
          'telefono' => '987654321',
          'rol_id' => 1,
          'estado' => true,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
      ]);
      DB::table('users')->insert([
        'correo' => 'coordinador@unsa.edu.pe',
        'contrasena' => Hash::make('password'), // password
        'dni' => '12345678',
        'nombre' => 'COR',
        'apellido_paterno' => 'COR',
        'apellido_materno' => 'COR',
        'direccion' => 'Av Parra',
        'telefono' => '987654321',
        'rol_id' => 2,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table('users')->insert([
        'correo' => 'operador@unsa.edu.pe',
        'contrasena' => Hash::make('password'), // password
        'dni' => '87654321',
        'nombre' => 'OP',
        'apellido_paterno' => 'OP',
        'apellido_materno' => 'OP',
        'direccion' => 'Av Parra',
        'telefono' => '987654321',
        'rol_id' => 3,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table('users')->insert([
        'correo' => 'director@unsa.edu.pe',
        'contrasena' => Hash::make('password'), // password
        'dni' => '87654322',
        'nombre' => 'DIRECTOR',
        'apellido_paterno' => 'OP',
        'apellido_materno' => 'OP',
        'direccion' => 'Av Parra',
        'telefono' => '987654321',
        'rol_id' => 4,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table('users')->insert([
        'correo' => 'adminlab@unsa.edu.pe',
        'contrasena' => Hash::make('password'), // password
        'dni' => '87654323',
        'nombre' => 'ADMIN LABORATORIO',
        'apellido_paterno' => 'AP',
        'apellido_materno' => 'AM',
        'direccion' => 'Av Parra',
        'telefono' => '987654321',
        'rol_id' => 6,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
      DB::table('users')->insert([
        'correo' => 'admininst@unsa.edu.pe',
        'contrasena' => Hash::make('password'), // password
        'dni' => '87654324',
        'nombre' => 'ADMIN INSTITUTO',
        'apellido_paterno' => 'AP',
        'apellido_materno' => 'AM',
        'direccion' => 'Av Parra',
        'telefono' => '987654321',
        'rol_id' => 7,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }
    // {
      
    // }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_institutos', function (Blueprint $table) {

            $table->id('proyecto_id');
            $table->unsignedInteger('instituto_id');
            $table->string('nombre_proyecto', 255);
            $table->string('investigador_principal', 255);
            $table->jsonb('coinvestigadores')->nullable();
            $table->string('nombre_imagen')->nullable();
            $table->string('imagen_referencial')->nullable();
            
            $table->string('etapa', 255)->nullable();
            $table->text('descripcion')->nullable();
            
            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_finalizacion')->nullable();

            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proyecto_institutos', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
        });

        Schema::dropIfExists('proyecto_institutos');
    }
};

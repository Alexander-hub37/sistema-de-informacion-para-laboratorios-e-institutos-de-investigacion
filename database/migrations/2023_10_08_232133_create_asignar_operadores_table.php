<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignar_operadores', function (Blueprint $table) {
            $table->id('asignar_id');
            $table->unsignedInteger('operador_id');
            $table->unsignedInteger('registro_id');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            //FOREIGN KEY en Users
            $table->foreign('operador_id')->references('usuario_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            //FOREIGN KEY en Registro
            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onUpdate('cascade')->onDelete('cascade');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('asignar_operadores', function (Blueprint $table) {
        $table->dropForeign(['registro_id']);
        $table->dropForeign(['operador_id']);
      });
      Schema::dropIfExists('asignar_operadores');
        // Schema::dropIfExists('asignar_operadores');
    }
};

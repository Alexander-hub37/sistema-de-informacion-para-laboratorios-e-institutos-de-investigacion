<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {

            $table->id('publicacion_id');
            $table->unsignedInteger('registro_id');
            $table->string('titulo');
            $table->text('link');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publicaciones', function (Blueprint $table) {
            $table->dropForeign(['registro_id']);
          });
        Schema::dropIfExists('publicaciones');
    }
};

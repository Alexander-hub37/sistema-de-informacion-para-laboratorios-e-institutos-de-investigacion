<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectoInstituto extends Model
{
    use HasFactory;

    protected $table = 'proyecto_institutos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'proyecto_id'; // Clave primaria personalizada

    protected $fillable = [
        'instituto_id',
        'nombre_proyecto',
        'investigador_principal',
        'coinvestigadores',
        'nombre_imagen',
        'imagen_referencial',
        'descripcion',
        'etapa',

        'fecha_inicio',
        'fecha_finalizacion',

        'estado',
        // 'imagen_desarrollo_proyecto_url',
    ];

    protected $casts = [
      'coinvestigadores' => 'array',
    ];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->imagen_referencial);
    }

}

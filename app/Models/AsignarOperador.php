<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsignarOperador extends Model
{
    use HasFactory;

    protected $table = 'asignar_operadores';
    protected $primaryKey = 'asignar_id';
    // Define las propiedades fillable para permitir la asignación en masa
    protected $fillable = [
        'operador_id',
        'registro_id',
        'estado'
    ];

    public function operador(){
      return $this->hasOne(User::class, 'usuario_id', 'operador_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Rol;

class Usuario extends Model
{
    use HasFactory;
    // Nombre de la tabla en la base de datos
    protected $table = 'usuarios';
    // Define las propiedades fillable para permitir la asignación en masa
    protected $primaryKey = 'idusuario';
    protected $fillable = [
        'documento',
        'nombreusuario',
        'apellidopa',
        'apellidoma',
        'direccionusuario',
        'telefonousuario',
        'correo',
        'passw',
        'idrol',
        'estado'
    ];
    protected $hidden = ['created_at', 'updated_at'];
    public function rol(){
        return $this->hasOne(Rol::class, 'idrol', 'idrol');
    }
}

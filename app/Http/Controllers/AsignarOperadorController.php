<?php

namespace App\Http\Controllers;

use App\Models\AsignarOperador;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AsignarOperadorController extends Controller
{
    // Listar los operadores de un laboratorio
    public function listarOperadoresLaboratorio($idregistrarlaboratorio) {

      $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
      //Verificar que el idlaboratorio es de tipo integer
      if($idregistrarlaboratorio === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }
      
      $operadores = AsignarOperador::where('registro_id', $idregistrarlaboratorio)->orderBy('asignar_id', 'asc')
                                  ->with('operador')
                                  ->get();

      if(count($operadores) == 0) {
        return response()->json(['message' => 'El laboratorio no tiene operadores asignados' ], 404);
      }

      return response()->json(['operadores'=>$operadores], 200);

    }

    public function asignarOperadorLaboratorio(Request $request, $idregistrarlaboratorio) {
      $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
      //Verificar que el idlaboratorio es de tipo integer
      if($idregistrarlaboratorio === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
      }

      $request->validate([
        'operadores' => 'required',
      ]);

      foreach ($request->operadores as $operador) {
        $asignarOperador = new AsignarOperador;
        $asignarOperador->operador_id = $operador;
        $asignarOperador->registro_id = $idregistrarlaboratorio;
        $asignarOperador->save();
      }
      return response()->json(['message'=>'Operadores asignados correctamente']);
    }
}

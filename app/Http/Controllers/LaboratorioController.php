<?php

namespace App\Http\Controllers;

use App\Models\Laboratorio;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LaboratorioController extends Controller
{
    /**
     * Mostrar laboratorios
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laboratorios = Laboratorio::orderBy('laboratorio_id', 'asc')->get();
        return response()->json(['laboratorios' => $laboratorios]);
    }

    /**
     * Registrar laboratorio
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(!empty(trim($request->get('nombre')))){
                $laboratorio = new Laboratorio;
                $laboratorio->nombre = $request->get('nombre');
                $laboratorio->save();
                return response()->json(['status' => 'Creado Correctamente']);
            }else{
                return response()->json(['message' => 'El campo nombre es requerido']);
            }
        }catch (QueryException $e){
            return response()->json(['message'=> 'La laboratorio ya esta registrado']);
        }
    }

    /**
     * Mostrar laboratorio por id
     *
     * @param  int $idlaboratorio
     * @return \Illuminate\Http\Response
     */
    public function show($idlaboratorio)
    {
        $idlaboratorio = (int) $idlaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
        if($idlaboratorio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $laboratorio = Laboratorio::where('laboratorio_id','=',$idlaboratorio)->first();
        if($laboratorio){
            return response()->json(['laboratorio' => $laboratorio]);
        } else {
            return response()->json(['message' => 'Laboratorio no encontrado'], 404);
        }
    }

    /**
     * Actualizar laboratorio  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idlaboratorio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idlaboratorio)
    {
        $idlaboratorio = (int) $idlaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
        if($idlaboratorio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $laboratorio = Laboratorio::where('laboratorio_id','=',$idlaboratorio)->first();
        if($laboratorio){
            try{
                if(!empty(trim($request->get('nombre')))){
                    $laboratorio->nombre = $request->get('nombre');
                    $laboratorio->update();
                    return response()->json(['status' => 'Actualizado Correctamente']);
                }else{
                    return response()->json(['message' => 'El campo nombre es requerido']);
                }
            } catch (QueryException $e){
                return response()->json(['message'=> 'La laboratorio ya está registrado']);
            }
        } else {
            return response()->json(['message' => 'Laboratorio no encontrado'], 404);
        }
    }

    /**
     * Eliminar laboratorio por id
     *
     * @param  int $idlaboratorio
     * @return \Illuminate\Http\Response
     */
    public function delete($idlaboratorio)
    {
        $idlaboratorio = (int) $idlaboratorio;
        //Verificar que el idlaboratorio es de tipo integer
        if($idlaboratorio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $laboratorio = Laboratorio::where('laboratorio_id','=',$idlaboratorio)->first();
        if($laboratorio && $laboratorio->estado){
            $laboratorio->estado = false;
            $laboratorio->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Laboratorio no encontrado'], 404);
        }
    }
}

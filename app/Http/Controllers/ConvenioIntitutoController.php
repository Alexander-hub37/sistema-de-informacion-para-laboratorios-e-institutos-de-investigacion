<?php

namespace App\Http\Controllers;

use App\Models\ConvenioInstituto;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ConvenioIntitutoController extends Controller
{
    //

    public function index($instituto_id)
    {
        try {
            // Hacemos la consulta a la base de datos mediante el id de instituto
            $convenios = $instituto_id
                ? ConvenioInstituto::where('instituto_id', $instituto_id)->get()
                : ConvenioInstituto::all();

            // Retornamos en formato Json la respuesta
            return response()->json(['convenios' => $convenios]);
        } catch (\Exception $e) {
            // Manejar la excepción y retornar una respuesta de error
            return response()->json(['error' => 'Error en la consulta: ' . $e->getMessage()], 500);
        }
    }
    public function store(Request $request)
    {   //Validamos las entradas de request
        try {
            $request->validate([
                'instituto_id' => 'required',
                'entidad' => 'required',
                'fecha_inicio' => 'required|date',
                'fecha_fin' => 'required|date',
                'objetivo' => 'required',
                'detalles' => 'required',
            ]);
            //Ingresamos los valores mediante el modelo
            $convenio = new ConvenioInstituto;
            $convenio->instituto_id = $request->instituto_id;
            $convenio->entidad = $request->entidad;
            $convenio->fecha_inicio = $request->fecha_inicio;
            $convenio->fecha_fin = $request->fecha_fin;
            $convenio->objetivo = $request->objetivo;
            $convenio->detalles = $request->detalles;
            //Guardamos los valores
            $convenio->save();
            //Enviamos mensaje de exito con el codigo 201
            return response()->json(['convenio' => $convenio, 'message' => 'Convenio creado correctamente'], 201);
        } catch (\Exception $e) {
            //Envia mensaje de error con el codigo 500
            return response()->json(['error' => 'Error al guardar el convenio: ' . $e->getMessage()], 500);
        }
    }

    public function show($idconvenio)
    {
        $idconvenio = (int) $idconvenio;
        //Verificar que el idinstituto es de tipo integer
        if($idconvenio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        // Obtener la función por ID con su relación con el instituto
        $convenio = ConvenioInstituto::where('convenio_id', '=', $idconvenio)->first();

        // Verificar si la función existe
        if($convenio){
            return response()->json(['convenio' => $convenio]);
        } else {
            return response()->json(['message' => 'Convenio no encontrado'], 404);
        }
    }

    public function update(Request $request, $idconvenio)
    {
        $idconvenio = (int) $idconvenio;
        //Verificar que el idinstituto es de tipo integer
        if ($idconvenio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $convenio = ConvenioInstituto::where('convenio_id', '=', $idconvenio)->first();
        if ($convenio) {
            try {
                //Se guardan los valores
                $convenio->instituto_id = $request->instituto_id;
                $convenio->entidad = $request->entidad;
                $convenio->fecha_inicio = $request->fecha_inicio;
                $convenio->fecha_fin = $request->fecha_fin;
                $convenio->objetivo = $request->objetivo;
                $convenio->detalles = $request->detalles;
                $convenio->update();
                //Se envia mensaje de exito
                return response()->json(['status' => 'Actualizado Correctamente']);
            } catch (QueryException $e) {
                //Se envia mensaje de error
                return response()->json(['message' => 'Error al actualizar convenio']);
            }
        } else {
            return response()->json(['message' => 'Convenio no encontrado'], 404);
        }
    }

    public function delete($idconvenio)
    {
        $idconvenio = (int) $idconvenio;
        //Verificar que el idinstituto es de tipo integer
        if ($idconvenio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        //Buscamos el convenio
        $convenio = ConvenioInstituto::where('convenio_id', '=', $idconvenio)->first();
        if ($convenio && $convenio->estado) {
            try {
                //Cambiamos el estado a false 
                $convenio->estado = false;
                $convenio->save();
                //Enviamos mensaje de exito
                return response()->json(['status' => 'Eliminado Correctamente']);
            } catch (QueryException $e) {
                //Enviamos mensaje de error
                return response()->json(['message' => 'Error al eliminar convenio']);
            }
        } else {
            return response()->json(['message' => 'Convenio no encontrado'], 404);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Disciplina;
use App\Models\Rol;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class DisciplinaController extends Controller
{
    /**
     * Mostrar disciplinas
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disciplinas = Disciplina::orderBy('disciplina_id', 'asc')->get();
        return response()->json(['disciplinas' => $disciplinas]);
    }

    /**
     * Registrar disciplina
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(!empty(trim($request->get('nombre')))){
                $disciplina = new Disciplina;
                $disciplina->nombre = $request->get('nombre');
                $disciplina->save();
                return response()->json(['status' => 'Creado Correctamente']); 
            }else{
                return response()->json(['message' => 'El campo nombre es requerido']);
            }
        }catch (QueryException $e){
            return response()->json(['message'=> 'La disciplina ya esta registrada']);
        }
    }

    /**
     * Mostrar disciplina por id
     *
     * @param  int $iddisciplina
     * @return \Illuminate\Http\Response
     */
    public function show($iddisciplina)
    {
        $iddisciplina = (int) $iddisciplina;
        //Verificar que el iddisciplina es de tipo integer
        if($iddisciplina === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $disciplina = Disciplina::where('disciplina_id','=',$iddisciplina)->first();
        if($disciplina && $disciplina->estado){
            return response()->json(['disciplina' => $disciplina]);
        } else {
            return response()->json(['message' => 'Disciplina no encontrada'], 404);
        }
    }

    /**
     * Actualizar disciplina  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $iddisciplina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $iddisciplina)
    {
        $iddisciplina = (int) $iddisciplina;
        //Verificar que el iddisciplina es de tipo integer
        if($iddisciplina === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $disciplina = Disciplina::where('disciplina_id','=',$iddisciplina)->first();
        if($disciplina && $disciplina->estado){
            try{
                if(!empty(trim($request->get('nombre')))){
                    $disciplina->nombre = $request->get('nombre');
                    $disciplina->update();
                    return response()->json(['status' => 'Actualizado Correctamente']); 
                }else{
                    return response()->json(['message' => 'El campo nombre es requerido']);
                }
            } catch (QueryException $e){
                return response()->json(['message'=> 'La disciplina ya está registrada']);
            }
        } else {
            return response()->json(['message' => 'Disciplina no encontrada'], 404);
        }
    }

    /**
     * Eliminar disciplina por id
     * @param  int $iddisciplina
     * @return \Illuminate\Http\Response
     */
    public function delete($iddisciplina)
    {
        $iddisciplina = (int) $iddisciplina;
        //Verificar que el iddisciplina es de tipo integer
        if($iddisciplina === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $disciplina = Disciplina::where('disciplina_id','=',$iddisciplina)->first();
        if($disciplina && $disciplina->estado){
            $disciplina->estado = false;
            $disciplina->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Disciplina no encontrada'], 404);
        }
    }
}

<?php

namespace App\Http\Middleware;

use App\Models\Rol;
use Closure;
use Illuminate\Http\Request;

class DirectorValidationMiddelware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        $rol = Rol::where('rol_id','=',$user->rol_id)->first();

        // Verifica si el usuario en sesión existe y si tiene un rol asignado
        if (!$user || !$user->rol_id) {
            return response()->json(['message' => 'El usuario en sesión no tiene un rol asignado'], 403);
        }

        if (strtolower($rol->nombre) !== 'director' && strtolower($rol->nombre) !== 'admininstitutos') {
            return response()->json(['message' => 'El usuario no tiene permisos para realizar esta acción'], 401);
        }

        return $next($request);
    }
}

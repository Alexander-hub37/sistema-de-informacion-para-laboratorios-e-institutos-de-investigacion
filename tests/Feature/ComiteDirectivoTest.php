<?php

namespace Tests\Feature;

use App\Models\Rol;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ComiteDirectivoTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba

    public function testIndexComiteDirectivo()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Comite Directivo', 'estado' => true]);

        User::factory()->create([
            'correo' => 'juan.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
        ]);

        $response = $this->get('api/comiteDirectivo');

        $response->assertStatus(200);
    }

    public function testStoreComiteDirectivo()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Rol de prueba', 'estado' => true]);

        $data = [
            'documento' => '35775312',
            'nombreusuario' => 'Juan Pérez',
            'apellidopa' => 'Pérez',
            'apellidoma' => 'López',
            'direccionusuario' => 'Calle 123, 4567',
            'telefonousuario' => '954786213',
            'email' => 'juan.perez@example.com',
            'password' => Hash::make("password"),
            'idrol' => $rol->rol_id,
            'estado' => true,
            'categoria' => "Principal",
            'regimen' => "Tiempo completo",
        ];

        $response = $this->post('api/comiteDirectivo', $data);

        $response->assertStatus(201);
    }

    public function testUpdateComiteDirectivo()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Rol de prueba', 'estado' => true]);

        $user = User::factory()->create([
            'correo' => 'juan.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
        ]);

        $data = [
            'nombreusuario' => 'Juan Pérez',
            'apellidopa' => 'Pérez',
            'apellidoma' => 'López',
            'direccionusuario' => 'Calle 123, 4567',
            'telefonousuario' => '954786213',
            'password' => Hash::make("password"),
            'idrol' => $rol->rol_id,
            'estado' => true,
            'categoria' => "Principal",
            'regimen' => "Tiempo completo",
        ];

        $response = $this->put("api/comiteDirectivo/{$user->usuario_id}", $data);

        $response->assertStatus(200);
    }

    public function testShowComiteDirectivo()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Comite Directivo', 'estado' => true]);

        $user = User::factory()->create([
            'correo' => 'juan.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
        ]);

        $response = $this->get("api/comiteDirectivo/{$user->usuario_id}");

        $response->assertStatus(200);
    }

    public function testDeleteComiteDirectivo()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Comite Directivo', 'estado' => true]);

        $user = User::factory()->create([
            'correo' => 'juan.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
        ]);

        $response = $this->delete("api/comiteDirectivo/{$user->usuario_id}");

        $response->assertStatus(200);
    }

}

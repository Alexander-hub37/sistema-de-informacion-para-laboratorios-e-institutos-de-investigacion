<?php

namespace Tests\Feature;

use App\Models\Laboratorio;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LaboratorioTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba
    
    public function testIndex()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        // Insertar datos de prueba en la base de datos
        Laboratorio::factory()->create(['nombre' => 'Laboratorio 1', 'estado' => true]);
        Laboratorio::factory()->create(['nombre' => 'Laboratorio 2', 'estado' => true]);

        $response = $this->get('/api/laboratorios');

        $response->assertStatus(200)
            ->assertJsonStructure(['laboratorios']);
    }

    public function testStore()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $data = ['nombre' => 'Nueva Laboratorio'];

        $response = $this->post('/api/laboratorios', $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);
    }

    public function testShow()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio de prueba', 'estado' => true]);

        $response = $this->get("/api/laboratorios/{$laboratorio->laboratorio_id}");

        $response->assertStatus(200)
            ->assertJson(['laboratorio' => ['nombre' => 'Laboratorio de prueba']]);
    }

    public function testUpdate()
    {

         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio de prueba 2', 'estado' => true]);
        $data = ['nombre' => 'Laboratorio actualizada'];

        $response = $this->put("/api/laboratorios/{$laboratorio->laboratorio_id}", $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio a eliminar', 'estado' => true]);

        $response = $this->delete("/api/laboratorios/{$laboratorio->laboratorio_id}");

        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);
    }
}

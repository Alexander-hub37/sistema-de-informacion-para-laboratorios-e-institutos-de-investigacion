<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Instituto;
use App\Models\Rol;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class InstitutoTest extends TestCase
{

    public function testIndexByDirector()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

        // Datos simulados para el nuevo instituto
        Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'ubicacion' => 'Ubicación del nuevo instituto',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->get("api/institutos/director/{$user->usuario_id}");

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);
    }

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testIndex()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775312',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan1.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

         // Crea algunos institutos de prueba
        Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'ubicacion' => 'Ubicación del nuevo instituto',
            'estado' => true,
        ]);

        // Llama a la ruta de la API
        $response = $this->get('api/institutos');

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'institutos'
        $response->assertJsonStructure(['institutos']);
    }

    public function testStore()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

         // Datos simulados para el nuevo instituto
        $data = [
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'ubicacion' => 'Ubicación del nuevo instituto',
            'estado' => true,
        ];

        // Llama a la ruta de la API con datos simulados
        $response = $this->post('api/institutos', $data);

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'status'
        $response->assertJson(['status' => 'Creado Correctamente']);
    }

    public function testShow()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

        // Datos simulados para el nuevo instituto
        $instituto = Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'ubicacion' => 'Ubicación del nuevo instituto',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->get("api/institutos/show/{$instituto->instituto_id}");

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);
    }

    public function testCompletarInstituto()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

         // Datos simulados para el nuevo instituto
        $data = [
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
        ];

        // Datos simulados para el nuevo instituto
        $instituto = Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto2',
            'mision' => 'Misión del nuevo instituto2',
            'vision' => 'Visión del nuevo instituto2',
            'historia' => 'Historia del nuevo instituto2',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto2',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->post("api/directores/completar/{$instituto->instituto_id}", $data);

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'status'
        $response->assertJson(['status' => 'Actualizado correctamente']);
    }

    public function testUpdate()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

         // Datos simulados para el nuevo instituto
        $data = [
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm'
        ];

        // Datos simulados para el nuevo instituto
        $instituto = Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto2',
            'mision' => 'Misión del nuevo instituto2',
            'vision' => 'Visión del nuevo instituto2',
            'historia' => 'Historia del nuevo instituto2',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto2',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->put("api/institutos/{$instituto->instituto_id}", $data);

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);
    }

    public function testDelete()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

        // Datos simulados para el nuevo instituto
        $instituto = Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto2',
            'mision' => 'Misión del nuevo instituto2',
            'vision' => 'Visión del nuevo instituto2',
            'historia' => 'Historia del nuevo instituto2',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto2',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->delete("api/institutos/{$instituto->instituto_id}");
        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);
    }

    public function testVistaInstitutoDirector()
    {

        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['nombre' => 'Director', 'estado' => true]);

        $user = User::factory()->create([
            'dni' => '35775313',
            'nombre' => 'Juan Pérez',
            'apellido_paterno' => 'Pérez',
            'apellido_materno' => 'López',
            'direccion' => 'Calle 123, 4567',
            'telefono' => '954786213',
            'correo' => 'juan2.perez@example.com',
            'contrasena' => Hash::make("password"),
            'rol_id' => $rol->rol_id,
            'estado' => true,
        ]);

        // Datos simulados para el nuevo instituto
        Instituto::factory()->create([
            'nombre' => 'Nuevo Instituto',
            'mision' => 'Misión del nuevo instituto',
            'vision' => 'Visión del nuevo instituto',
            'historia' => 'Historia del nuevo instituto',
            'usuario_director' => $user->usuario_id,
            'comite_directivo' => 'Comite directivo del nuevo instituto',
            'contacto' => 'Contacto del nuevo instituto',
            'ubicacion' => 'Ubicacion del nuevo instituto',
            'url_instituto' => 'https://github.com/nvm-sh/nvm',
            'url_facebook' => 'https://github.com/nvm-sh/nvm',
            'estado' => true,
        ]);

        // Llama a la ruta de la API con datos simulados
        $response = $this->get("api/institutosPorDirector/{$user->usuario_id}");

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);
    }

}

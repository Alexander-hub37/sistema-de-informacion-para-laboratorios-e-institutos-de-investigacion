<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Area;
use App\Models\Disciplina;
use App\Models\Laboratorio;
use App\Models\RegistroLaboratorio;
use App\Models\Rol;
use App\Models\User;


class LaboratorioPublicoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_puede_obtener_registro_laboratorio_publico()

    {

        $user = User::factory()->create();
        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio 1', 'estado' => true]);
        $area = Area::factory()->create(['nombre' => 'Área de prueba', 'estado' => true]);
        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina de prueba', 'estado' => true]);

        // Crear un registro de laboratorio 
        $registro = RegistroLaboratorio::factory()->create([
            'coordinador_id' => $user->usuario_id, // Reemplaza con valores apropiados
            'laboratorio_id' => $laboratorio->laboratorio_id,
            'area_id' => $area->area_id,
            'disciplina_id' => $disciplina->disciplina_id,
            'ubicacion' => "ubicacion prueba",
            'mision' => "mision prueba",
            'vision' => "vision prueba",
            'historia' => "historia prueba",
            'estado' => true,
            'servicios' => ['Servicio 1', 'Servicio 2', 'Servicio 3'],

        ]);

        // Hacer una solicitud GET a la ruta /registroLaboratorioPublico
        $response = $this->get('/registroLaboratorioPublico');

        // Verificar si la solicitud fue exitosa (código de estado 200)
        $response->assertStatus(200);
    }

    public function test_puede_obtener_laboratorio_por_id_con_coordinador_interno()
    {
        
        $user = User::factory()->create();
        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio 2', 'estado' => true]);
        $area = Area::factory()->create(['nombre' => 'Área de prueba 1', 'estado' => true]);
        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina de prueban 1', 'estado' => true]);
        // Crear un registro de laboratorio (puedes personalizar los datos según tus necesidades)
        $registro_coordinador = RegistroLaboratorio::factory()->create([
            'coordinador_id' => $user->usuario_id, // Reemplaza con valores apropiados
            'laboratorio_id' => $laboratorio->laboratorio_id,
            'area_id' => $area->area_id,
            'disciplina_id' => $disciplina->disciplina_id,
            'ubicacion' => "ubicacion prueba",
            'mision' => "mision prueba",
            'vision' => "vision prueba",
            'historia' => "historia prueba",
            'estado' => true,
            'servicios' => ['Servicio 1', 'Servicio 2', 'Servicio 3'],

        ]);

        // Hacer una solicitud GET a la ruta /laboratorioCoordinador/{idLaboratorio}
        $response = $this->get("/laboratorioCoordinador/{$registro_coordinador  ->registro_id}");

        // Verificar si la solicitud fue exitosa (código de estado 200)
        $response->assertStatus(200);

       
    }




}
